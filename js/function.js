const btn = document.getElementById('openModal');
const modal = document.getElementById('modal');
const closeBtn = document.getElementById('modalClose');

btn.addEventListener('click', function () {
    modal.style.display = 'block';
});

closeBtn.addEventListener('click', function () {
    modal.style.display = 'none';
});

const taskValue = document.getElementsByClassName('modal__value')[0];
const taskSubmit = document.getElementsByClassName('modal__submit')[0];
const priority = document.querySelector('modal__select')[0];
const deadline = document.querySelector('input[type="date"]')[0]; 
const taskList = document.getElementsByClassName('task__list')[0];

//追加ボタンを作成
const addTasks = (task) => {
    // 入力したタスクを追加・表示
    const listItem = document.createElement('li');
    const showItem = taskList.appendChild(listItem);
    showItem.innerHTML = task;
};

taskSubmit.addEventListener('click', evt => {
    evt.preventDefault();
    const task = taskValue.value;
    addTasks(task);
    taskValue.value = '';
});

// function to add todos to local storage
function addToLocalStorage(todos) {
    // conver the array to string then store it.
    localStorage.setItem('todos', JSON.stringify(todos));
    // render them to screen
    renderTodos(todos);
  }